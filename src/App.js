import React, { Component } from "react";
import { Router, Route, Switch } from "react-router-dom";
import { createBrowserHistory } from "history";
import app_one_routes from './apps/app_one/routes';
import Layout from './layout'
import './App.scss';
import Axios from "./services/Axios";
import config from './config.json';
const ax = new Axios()
window.axiosIns = ax.get();
const customHistory = createBrowserHistory();

class App extends Component {
    state = {
        loading: false,
        loggedin: false
    }
    componentDidMount() {
        this.setState({ loading: true });
        window.axiosIns.post('user/auth', {
            username: config.username,
            password: config.password
        }).then((response) => {
            if (response.status === 200) {
                localStorage.setItem('current_user', JSON.stringify(response.data));
                this.setState({ loggedin: true });
            }
            this.setState({ loading: false });
        }).catch((error) => {
            alert("Could not login.");
            this.setState({ loading: false });
        })
    }

    AppRouteBase = ({ component: Component, layout: Layout, ...rest }) => {
        return (
            <Route
                {...rest}
                render={props => {
                    return (
                        <Layout history={customHistory}>
                            <Component {...props} {...rest} />
                        </Layout>
                    );
                }}
            />
        );
    };

    render() {
        if (this.state.loading) { return <div>Loading</div>; }
        if (!this.state.loggedin){ return null; }
        const AppRoute = this.AppRouteBase;
        const rootRoute = app_one_routes.find(x => x.path === "/") || {};
        return (
            <Router history={customHistory}>
                <div style={{ position: "relative", height: "100%" }}>
                    <Switch>
                        {rootRoute ? <AppRoute exact path={rootRoute.path} layout={Layout} component={rootRoute.component} /> : null}
                        {
                            app_one_routes.map((route, index) => {
                                return <AppRoute exact path={`/app-one${route.path}`} key={index} layout={Layout} component={route.component} />
                            })
                        }
                    </Switch>
                </div>
            </Router>
        );
    }
}

export default App;
