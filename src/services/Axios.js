import axios from 'axios';

class Axios {
    constructor() {
        this.axiosIns = axios.create({
            baseURL: 'https://api.sensor.oronetworks.com/',
            headers: {}
        });

        this.axiosIns.interceptors.request.use(function (config) {
            if (localStorage.getItem('current_user') && JSON.parse(localStorage.getItem('current_user')).token) {
                config.headers['Authorization'] = `Bearer ${JSON.parse(localStorage.getItem('current_user')).token}`;
            }
            return config;
        }, function (error) {
            return Promise.reject(error);
        });

        this.axiosIns.interceptors.response.use(function (response) {
            return response;
        }, function (error) {
            return Promise.reject(error);
        });
    }

    get = () => {
        return this.axiosIns;
    }
}

export default Axios;
