import React from 'react';

const ComponentTwo = (props) => {
    return (
        <div>This is component two.</div>
    )
}

export default ComponentTwo;