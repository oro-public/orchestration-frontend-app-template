import React, { useEffect } from 'react';
import { Link } from 'react-router-dom'

const ComponentOne = (props) => {
    console.log("link:", Link )
    useEffect(() => {
        // axios example. always use "window.axiosIns" for API calls.
        window.axiosIns.get('https://jsonplaceholder.typicode.com/todos/1').then((value) => {
            debugger;
        }, [])
    })

    return (
        <div style={{padding: 10}}>
            This is component one.
            {/* Notice "/apps/app-one/" in below code. as documentation suggests, While implementing navigation inside your app always use final url (apps/<app-name>/my-route) in href value. */}
            <Link to="/app-one/component2">Go To Component 2</Link> 
            <div style={{marginTop: 10}}>This is the area where your app UI will be rendered.</div>
        </div>
    )
}

export default ComponentOne;