import ComponentOne from "./components/component1";
import ComponentTwo from "./components/component2";

const Routes = [
    {
        path: "/",
        component: ComponentOne,
    },
    {
        path: "/component1",
        component: ComponentOne,
    },
    {
        path: "/component2",
        component: ComponentTwo,
    }
];

export default Routes;
